OFILES_NITRO	:= fs.o os.o fxmath.o fx_sincos.o mi.o
OFILES_MPH	:= main.o io.o error.o heap.o \
		   model.o animation.o room.o dlist.o collision.o entity.o \
		   pickup_models.o texture_containers.o rooms.o \
		   state.o

OFILES		:= $(OFILES_NITRO) $(OFILES_MPH)

CC		:= gcc

CFLAGS		:= -O3 -std=c99 -m32 -g
CFLAGS		+= -D__NITRO_EMU__
LDFLAGS		:= -g -m32
LIBS		:= -lGL -lGLU -lglut -lm

.PHONY: all clean

all: main
	@echo "Build finished"

clean:
	@echo [CLEAN]
	@rm $(OFILES)


main: $(OFILES)
	@echo "[LD]        $(notdir $@)"
	$(CC) $(LDFLAGS) -o "$@" $(OFILES) $(LIBS)

%.o: %.c
	@echo "[CC]        $(notdir $@)"
	@$(CC) $(CFLAGS) -c -o "$@" "$<"
