#ifndef __ROOM_H__
#define __ROOM_H__

struct Room;
typedef struct Room Room;

#include "types.h"
#include "model.h"
#include "rooms.h"
#include "animation.h"
#include "collision.h"

#define NUM_NODE_REFS 32

enum NODE_LAYER_MASK
{
	LAYER_ML0 = 0x8,
	LAYER_ML1 = 0x10,
	LAYER_MPU = 0x20,
	LAYER_CTF = 0x4000
};

#define CModel int
#define CMaterial void

struct Room {
	int		some_value;
	char		name[16];
	int		field_14;
	int		field_18;
	MtxFx43		field_1C;
	VecFx32		pos;
	const RoomDescription* room_descr;
	int		field_5C;
	CModel		croom;
	CMaterial*	cmaterials;
	Model*		model;
	Animation*	animation;
	Collision*	collision;
	Room*		next;
};

struct NodeRef;
typedef struct NodeRef NodeRef;

struct NodeRef {
	char*		room_node_name;
	int		room_node_id_maybe;
	Port*		ports;
	s16		room_node_id;
	u16		field_E;
	Room*		room;
	void*		sth;
	NodeRef*	next;
	NodeRef*	prev;
};

/* global variables */
extern u16 room_layer_mask;
extern u8* current_texture_image;
extern u32 current_texture_size;
extern int modelview_mtx_shamt;

extern NodeRef* node_refs;
extern NodeRef nodes_list;
extern NodeRef room_node_refs;

int nodes_count;

/* functions */
void unlink_node_ref(NodeRef* node_ref);
void insert_node(NodeRef* prev, NodeRef* node_ref);
NodeRef* remove_first_node();
void get_room_node(NodeRef* node_ref, Room* room, char* room_node_name);
Room* load_room(const RoomDescription* room_descr, int which_list, int some_value, fx32 x, fx32 y, fx32 z, char* room_name);

void initialize_current_room();

#endif
