#ifndef __DLIST_H__
#define __DLIST_H__

#include "types.h"
#include "model.h"

void draw_dlist(DisplayList* dlist);
void geom_reset_bounds();
void geom_get_bounds(float* min_x, float* min_y, float* min_z, float* max_x, float* max_y, float* max_z);

#endif
