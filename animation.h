#ifndef __ANIMATION_H__
#define __ANIMATION_H__

#include "types.h"
#include "model.h"

typedef struct {
	u8	field_0;
	u8	field_1;
	u8	field_2;
	u8	flags;
	s16	field_4;
	s16	field_6;
	s16	field_8;
	u16	field_A;
	u16	field_C;
	u16	field_E;
	u8	field_10;
	u8	field_11;
	u8	field_12;
	u8	field_13;
	s16	field_14;
	s16	field_16;
	s16	field_18;
	u16	field_1A;
	u16	field_1C;
	u16	field_1E;
	u8	field_20;
	u8	field_21;
	u8	field_22;
	u8	field_23;
	s16	field_24;
	s16	field_26;
	s16	field_28;
	u16	field_2A;
	u16	field_2C;
	u16	field_2E;
} NodeAnimation;

typedef struct {
	u32	data;
	fx32*	fx32_ptr;
	u16*	short_ptr;
	u32*	int_ptr;
	NodeAnimation* node_animations;
} NodeAnimationData;

typedef struct {
	NodeAnimationData** node_animations;
	void**	field_4;
	MaterialAnimationData** material_animations;
	TexcoordAnimationData** texcoord_animations;
	TextureAnimationData** texture_animations;
	u16	count;
	s16	field_16;
} Animation;

/* functions */
void load_animation(Animation** animation, const char* filename, Model* model, char flags);

#endif
