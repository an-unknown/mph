#ifndef __FX_MATH_H__
#define __FX_MATH_H__

#include "types.h"

extern const fx16 FX_SinCosTable_[];

void VEC_Add(const VecFx32* a, const VecFx32* b, VecFx32* ab);
fx32 VEC_DotProduct(const VecFx32* a, const VecFx32* b);

void MTX_Identity43(MtxFx43* pDst);
void MTX_Concat43(const MtxFx43* a, const MtxFx43* b, MtxFx43* ab);

void MTX_RotZ44(MtxFx44 *pDst, fx32 sinVal, fx32 cosVal);

/* FX_CosIdx(idx) = sinf(2 * M_PI / 65536.0 * idx) */
inline fx16 FX_SinIdx(int idx)
{
	return FX_SinCosTable_[((idx >> 4) << 1)];
}

/* FX_CosIdx(idx) = cosf(2 * M_PI / 65536.0 * idx) */
inline fx16 FX_CosIdx(int idx)
{
	return FX_SinCosTable_[((idx >> 4) << 1) + 1];
}

#endif
