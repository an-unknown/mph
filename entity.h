#ifndef __ENTITY_H__
#define __ENTITY_H__

enum ENTITY_TYPE {
	PICKUP_0 = 0x0,
	OBJECT = 0x1,
	ALIMBIC_DOOR = 0x3,
	PICKUP_1 = 0x4,
	PICKUP_2 = 0x6,
	JUMP_PAD = 0x9,
	TELEPORTER = 0xE,
	ARTIFACT = 0x11,
	CAMERA_SEQ = 0x12,
	FORCE_FIELD = 0x13,
};

typedef struct {
	u16		type;
} EntityData;

typedef struct {
	u8		field_0;
	u8		field_1;
	s16		field_2;
	u32		field_4;
	u32		field_8;
	u32		field_C;
	s16		layer_mask;
	u16		length;
	EntityData*	data;
} Entity;

typedef struct {
	u32		version;
	u16		lengths[16];
} EntityFileHeader;


extern Entity* entities;

void EntLoad(Entity** ent, const char* filename, int layer_id);

#endif
