#ifndef __COLLISION_H__
#define __COLLISION_H__

#include "types.h"

typedef struct {
	VecFx32		n;
	fx32		w;
} CollisionPlane;

typedef struct {
	u32		field_0;
	u32		field_4;
	u16		layer_mask;
	s16		field_A;
	u32		field_C;
} CollisionData;

typedef struct {
	u16		cnt;
	u16		start_idx;
} CollisionEntry;

typedef struct {
	char		port_name[8];
	u32		field_8;
	u32		field_C;
	u32		field_10;
	u32		field_14;
	u32		field_18;
	u32		field_1C;
	u32		field_20;
	u32		field_24;
	char		port_name_1[24];
	char		port_name_2[24];
	VecFx32		vec4;
	VecFx32		vec3;
	VecFx32		vec2;
	VecFx32		vec1;
	VecFx32		vec5;
	u32		field_94;
	VecFx32		vec6;
	u32		field_A4;
	VecFx32		vec7;
	u32		field_B4;
	VecFx32		vec8;
	u32		field_C4;
	VecFx32		vec9;
	u32		field_D4;
	s16		flags;
	s16		layer_mask;
	s16		field_DC;
	s16		field_DE;
} CollisionPort;

typedef struct {
	u32		magic;
	u32		vector_count;
	VecFx32*	vectors;
	u32		plane_count;
	CollisionPlane*	planes;
	u32		field_14;
	void*		field_18;
	u32		field_1C;
	CollisionData*	data;
	u32		field_24;
	u16*		indices1;
	u32		field_2C;
	u32		field_30;
	u32		field_34;
	fx32		x;
	fx32		y;
	fx32		z;
	u32		count;
	CollisionEntry*	entries;
	u32		port_count;
	CollisionPort*	ports;
} Collision;

struct Port;
typedef struct Port Port;

struct Port {
	s16		side;
	s16		geo_node_id;
	CollisionPort*	port;
	Port*		next;
};

#include "room.h"

/* functions */
void load_collision(Collision** collision, const char* filename, char flags);
void translate_collision_data(Collision* collision, fx32 x, fx32 y, fx32 z);
void setup_port_nodes(CollisionPort* collision_port, Room* room);

#endif
