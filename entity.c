#include <string.h>

#include "types.h"
#include "error.h"
#include "fs.h"
#include "heap.h"
#include "entity.h"

Entity* entities;

void EntLoad(Entity** ent, const char* filename, int layer_id)
{
	unsigned int i;
	FSFile file;
	EntityFileHeader header;

	FS_InitFile(&file);
	if(!FS_OpenFile(&file, filename))
		fatal_error("EntLoad: Cannot open file %s!\n", filename);
	if(FS_ReadFile(&file, &header, sizeof(EntityFileHeader)) != sizeof(EntityFileHeader))
		fatal_error("EntLoad: Failed to read the file %s\n", filename);
	if(header.version != 2)
		fatal_error("EntLoad(): Error! Entity file \"%s\" is the wrong version!\n", filename);

	unsigned int length = header.lengths[layer_id];
	*ent = (Entity*) alloc_from_heap((length + 1) * sizeof(Entity));

	for(i = 0; i < length; i++) {
		Entity* entity = &(*ent)[i];
		if(FS_ReadFile(&file, entity, sizeof(Entity)) != sizeof(Entity))
			fatal_error("EntLoad: Failed to read the file %s!\n", filename);
		if(entity->layer_mask & (1 << layer_id)) {
			int offset = FS_GetPosition(&file);
			FS_SeekFile(&file, (int) entity->data, FS_SEEK_SET);
			entity->data = (EntityData*) alloc_from_heap(entity->length);
			if(FS_ReadFile(&file, entity->data, entity->length) != entity->length)
				fatal_error("EntLoad: Failed to read the file %s!\n", filename);
			FS_SeekFile(&file, offset, FS_SEEK_SET);
		}
	}

	Entity* end = &(*ent)[length];
	memset(end, 0, sizeof(Entity));

	FS_CloseFile(&file);
}
