#include <stdint.h>
#include <string.h>

#include "types.h"
#include "os.h"
#include "heap.h"
#include "io.h"
#include "fxmath.h"
#include "collision.h"
#include "model.h"
#include "room.h"

#define MAKEPTR(x, base) { \
	if(x) { \
		(x) = (void*) ((uintptr_t) (x) + (uintptr_t) (base)); \
	} \
}

static void parse_collision_data(Collision* collision)
{
	unsigned int i;
	unsigned int j;
	u16 buf[0x200];

	for(i = 0; i < collision->count; i++) {
		CollisionEntry* entry = &collision->entries[i];
		if(entry->cnt && entry->cnt < 0x200) {
			unsigned int off = 0;
			for(j = 0; j < entry->cnt; j++) {
				int idx = collision->indices1[entry->start_idx + j];
				int layer_mask = collision->data[idx].layer_mask;
				if(layer_mask & 4 || layer_mask & room_layer_mask)
					buf[off++] = idx;
			}
			if(off)
				memcpy(&collision->indices1[entry->start_idx], buf, off * sizeof(short));
			entry->cnt = off;
		}
	}
}

static void parse_collision(Collision* collision)
{
	if(collision->magic != *(u32*)"wc01")
		OS_Terminate();

	MAKEPTR(collision->vectors, collision);
	MAKEPTR(collision->planes, collision);
	MAKEPTR(collision->field_18, collision);
	MAKEPTR(collision->data, collision);
	MAKEPTR(collision->indices1, collision);
	MAKEPTR(collision->entries, collision);
	MAKEPTR(collision->ports, collision);

	parse_collision_data(collision);
}

void load_collision(Collision** collision, const char* filename, char flags)
{
	if(flags & USE_ARCHIVE)
		LoadFileFromArchive((void**) collision, filename);
	else
		LoadFile((void**) collision, filename);

	parse_collision(*collision);
}

void translate_collision_data(Collision* collision, fx32 x, fx32 y, fx32 z)
{
	unsigned int i;
	VecFx32 vec;
	VecFx32 b;

	vec.x = x;
	vec.y = y;
	vec.z = z;

	for(i = 0; i < collision->vector_count; i++)
		VEC_Add(&collision->vectors[i], &vec, &collision->vectors[i]);

	for(i = 0; i < collision->plane_count; i++) {
		CollisionPlane* plane = &collision->planes[i];

		fx32 nx = plane->n.x;
		fx32 ny = plane->n.y;
		fx32 nz = plane->n.z;
		fx32 w = plane->w;

		b.x = vec.x + FX_MUL(nx, w);
		b.y = vec.y + FX_MUL(ny, w);
		b.z = vec.z + FX_MUL(nz, w);

		plane->w = VEC_DotProduct(&plane->n, &b);
	}
}

void setup_port_nodes(CollisionPort* collision_port, Room* room)
{
	unsigned int i;
	char buf[32];

	collision_port->flags = collision_port->flags & 0xFFFE | 1;

	char* name_1 = collision_port->port_name_1;
	char* name_2 = collision_port->port_name_2;

	for(i = 0; i < 2; i++) {
		char* name = i ? name_2 : name_1;
		NodeRef* ref;
		NodeRef* node_ref = NULL;
		for(ref = room_node_refs.next; ref->next != &room_node_refs; ref = ref->next) {
			if(!strcmp(ref->room_node_name, name)) {
				node_ref = ref;
				break;
			}
		}

		if(!node_ref) {
			node_ref = remove_first_node();
			get_room_node(node_ref, room, name);
		}

		Port* port = (Port*) alloc_from_heap(sizeof(Port));
		port->geo_node_id = -1;
		if(*(u32*) collision_port->port_name == *(u32*) "gamp") {
			sprintf(buf, "geo%s", &collision_port->port_name[1]);
			port->geo_node_id = get_node_child(buf, room->model);
		}

		port->port = collision_port;
		port->side = i;
		port->next = node_ref->ports;
		node_ref->ports = port;
	}
}
