#ifndef __ROOMS_H__
#define __ROOMS_H__

#include "types.h"

#define NUM_ROOMS 122

typedef struct {
	char*	name;
	char*	model;
	char*	anim;
	char*	tex;
	char*	collision;
	char*	ent;
	char*	node;
	char*	room_node_name;
	u32	battle_time_limit;
	u32	time_limit;
	s16	point_limit;
	s16	layer_id;
	u32	unk8;
	u16	fog_enable;
	u16	fog;
	u16	fog_color;
	s16	field_36;
	u32	fog_slope;
	u32	fog_offset;
	u32	unk13;
	VecFx32	light_1;
	u32	unk17;
	VecFx32	light_2;
	char*	internal_name;
	char*	archive;
	u32	unk21;
	u32	unk22;
} RoomDescription;

extern const RoomDescription rooms[NUM_ROOMS];

#endif
