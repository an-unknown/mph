#include <float.h>
#include <GL/gl.h>

#include "types.h"
#include "error.h"
#include "dlist.h"
#include "model.h"

static float geom_min_x;
static float geom_min_y;
static float geom_min_z;
static float geom_max_x;
static float geom_max_y;
static float geom_max_z;

static inline void update_bounds(float vtx_state[3])
{
	if(vtx_state[0] < geom_min_x)
		geom_min_x = vtx_state[0];
	if(vtx_state[0] > geom_max_x)
		geom_max_x = vtx_state[0];

	if(vtx_state[1] < geom_min_y)
		geom_min_y = vtx_state[1];
	if(vtx_state[1] > geom_max_y)
		geom_max_y = vtx_state[1];

	if(vtx_state[2] < geom_min_z)
		geom_min_z = vtx_state[2];
	if(vtx_state[2] > geom_max_z)
		geom_max_z = vtx_state[2];
}

static void do_reg(u32 reg, u32** data_p, float vtx_state[3])
{
	u32* data = *data_p;

	switch(reg) {
		/* NOP */
		case 0x400:
			break;

		/* MTX_RESTORE */
		case 0x450: {
			u32 idx = *(data++);
			break;
		}

		/* COLOR */
		case 0x480: {
			u32 rgb = *(data++);
			u32 r = (rgb >>  0) & 0x1F;
			u32 g = (rgb >>  5) & 0x1F;
			u32 b = (rgb >> 10) & 0x1F;
			glColor3f(((float) r) / 31.0f, ((float) g) / 31.0f, ((float) b) / 31.0f);
			break;
		}

		/* NORMAL */
		case 0x484: {
			u32 xyz = *(data++);
			s32 x = (xyz >>  0) & 0x3FF;	if(x & 0x200)	x |= 0xFFFFFC00;
			s32 y = (xyz >> 10) & 0x3FF;	if(y & 0x200)	y |= 0xFFFFFC00;
			s32 z = (xyz >> 20) & 0x3FF;	if(z & 0x200)	z |= 0xFFFFFC00;
			glNormal3f(((float) x) / 512.0f, ((float) y) / 512.0f, ((float) z) / 512.0f);
			break;
		}

		/* TEXCOORD */
		case 0x488: {
			u32 st = *(data++);
			s32 s = (st >>  0) & 0xFFFF;	if(s & 0x8000)	s |= 0xFFFF0000;
			s32 t = (st >> 16) & 0xFFFF;	if(t & 0x8000)	t |= 0xFFFF0000;
			glTexCoord2f(((float) s) / 16.0f, ((float) t) / 16.0f);
			break;
		}

		/* VTX_16 */
		case 0x48C: {
			u32 xy = *(data++);
			s32 x = (xy >>  0) & 0xFFFF;	if(x & 0x8000)	x |= 0xFFFF0000;
			s32 y = (xy >> 16) & 0xFFFF;	if(y & 0x8000)	y |= 0xFFFF0000;
			s32 z = *(data++)  & 0xFFFF;	if(z & 0x8000)	z |= 0xFFFF0000;

			vtx_state[0] = ((float) x) / 4096.0f;
			vtx_state[1] = ((float) y) / 4096.0f;
			vtx_state[2] = ((float) z) / 4096.0f;

			glVertex3fv(vtx_state);
			update_bounds(vtx_state);
			break;
		}

		/* VTX_10 */
		case 0x490: {
			u32 xyz = *(data++);
			s32 x = (xyz >>  0) & 0x3FF;	if(x & 0x200)	x |= 0xFFFFFC00;
			s32 y = (xyz >> 10) & 0x3FF;	if(y & 0x200)	y |= 0xFFFFFC00;
			s32 z = (xyz >> 20) & 0x3FF;	if(z & 0x200)	z |= 0xFFFFFC00;

			vtx_state[0] = ((float) x) / 4096.0f;
			vtx_state[1] = ((float) y) / 4096.0f;
			vtx_state[2] = ((float) z) / 4096.0f;

			glVertex3fv(vtx_state);
			update_bounds(vtx_state);
			break;
		}

		/* VTX_XY */
		case 0x494: {
			u32 xy = *(data++);
			s32 x = (xy >>  0) & 0xFFFF;	if(x & 0x8000)	x |= 0xFFFF0000;
			s32 y = (xy >> 16) & 0xFFFF;	if(y & 0x8000)	y |= 0xFFFF0000;

			vtx_state[0] = ((float) x) / 4096.0f;
			vtx_state[1] = ((float) y) / 4096.0f;

			glVertex3fv(vtx_state);
			update_bounds(vtx_state);
			break;
		}

		/* VTX_XZ */
		case 0x498: {
			u32 xz = *(data++);
			s32 x = (xz >>  0) & 0xFFFF;	if(x & 0x8000)	x |= 0xFFFF0000;
			s32 z = (xz >> 16) & 0xFFFF;	if(z & 0x8000)	z |= 0xFFFF0000;

			vtx_state[0] = ((float) x) / 4096.0f;
			vtx_state[2] = ((float) z) / 4096.0f;

			glVertex3fv(vtx_state);
			update_bounds(vtx_state);
			break;
		}

		/* VTX_YZ */
		case 0x49C: {
			u32 yz = *(data++);
			s32 y = (yz >>  0) & 0xFFFF;	if(y & 0x8000)	y |= 0xFFFF0000;
			s32 z = (yz >> 16) & 0xFFFF;	if(z & 0x8000)	z |= 0xFFFF0000;

			vtx_state[1] = ((float) y) / 4096.0f;
			vtx_state[2] = ((float) z) / 4096.0f;

			glVertex3fv(vtx_state);
			update_bounds(vtx_state);
			break;
		}

		/* VTX_DIFF */
		case 0x4A0: {
			u32 xyz = *(data++);
			s32 x = (xyz >>  0) & 0x3FF;	if(x & 0x200)	x |= 0xFFFFFC00;
			s32 y = (xyz >> 10) & 0x3FF;	if(y & 0x200)	y |= 0xFFFFFC00;
			s32 z = (xyz >> 20) & 0x3FF;	if(z & 0x200)	z |= 0xFFFFFC00;

			vtx_state[0] += ((float) x) / 4096.0f;
			vtx_state[1] += ((float) y) / 4096.0f;
			vtx_state[2] += ((float) z) / 4096.0f;

			glVertex3fv(vtx_state);
			update_bounds(vtx_state);
			break;
		}

		/* DIF_AMB */
		case 0x4C0: {
			u32 val = *(data++);
			break;
		}

		/* BEGIN_VTXS */
		case 0x500: {
			u32 type = *(data++);
			switch(type) {
				case 0:	glBegin(GL_TRIANGLES);		break;
				case 1:	glBegin(GL_QUADS);		break;
				case 2:	glBegin(GL_TRIANGLE_STRIP);	break;
				case 3:	glBegin(GL_QUAD_STRIP);		break;
				default:fatal_error("Unknown geom type 0x%x\n", type);
			}
			break;
		}

		default:
			fatal_error("Unhandled reg write: 0x%x\n", reg);
	}

	*data_p = data;
}

static void do_dlist(u32* data, u32 size)
{
	u32* end = data + size / 4;

	float vtx_state[3] = { 0.0f, 0.0f, 0.0f };

	while(data  <end) {
		u32 regs = *(data++);

		u32 c;
		for(c = 0; c < 4; c++, regs >>= 8) {
			u32 reg = ((regs & 0xFF) << 2) + 0x400;

			do_reg(reg, &data, vtx_state);
		}
	}
}

void draw_dlist(DisplayList* dlist)
{
	do_dlist(dlist->data, dlist->size);
}

void geom_reset_bounds()
{
	geom_min_x = FLT_MAX;
	geom_min_y = FLT_MAX;
	geom_min_z = FLT_MAX;
	geom_max_x = -FLT_MAX;
	geom_max_y = -FLT_MAX;
	geom_max_z = -FLT_MAX;
}

void geom_get_bounds(float* min_x, float* min_y, float* min_z, float* max_x, float* max_y, float* max_z)
{
	*min_x = geom_min_x;
	*min_y = geom_min_y;
	*min_z = geom_min_z;
	*max_x = geom_max_x;
	*max_y = geom_max_y;
	*max_z = geom_max_z;
}
