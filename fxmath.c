#include <string.h>

#include "fxmath.h"

void VEC_Add(const VecFx32* a, const VecFx32* b, VecFx32* ab)
{
	ab->x = a->x + b->x;
	ab->y = a->y + b->y;
	ab->z = a->z + b->z;
}

fx32 VEC_DotProduct(const VecFx32* a, const VecFx32* b)
{
	return (fx32)(((fx64)a->x * b->x + (fx64)a->y * b->y + (fx64)a->z * b->z + (1 << (FX64_SHIFT - 1))) >> FX64_SHIFT);
}

void MTX_Identity43(MtxFx43* pDst)
{
	memset(pDst, 0, sizeof(MtxFx43));
	pDst->_00 = FX32_CONST(1.0);
	pDst->_11 = FX32_CONST(1.0);
	pDst->_22 = FX32_CONST(1.0);
}

void MTX_Concat43(const MtxFx43* a, const MtxFx43* b, MtxFx43* ab)
{
	MtxFx43 tmp;
	MtxFx43* p;

	fx32 x, y, z;
	fx32 xx, yy, zz;

	if(ab == b)
		p = &tmp;
	else
		p = ab;

	// compute (a x b) -> p

	// row 0
	x = a->_00;
	y = a->_01;
	z = a->_02;

	p->_00 = (fx32)(((fx64)x * b->_00 + (fx64)y * b->_10 + (fx64)z * b->_20) >> FX32_SHIFT);
	p->_01 = (fx32)(((fx64)x * b->_01 + (fx64)y * b->_11 + (fx64)z * b->_21) >> FX32_SHIFT);

	xx = b->_02;
	yy = b->_12;
	zz = b->_22;

	p->_02 = (fx32)(((fx64)x * xx + (fx64)y * yy + (fx64)z * zz) >> FX32_SHIFT);

	// row 1
	x = a->_10;
	y = a->_11;
	z = a->_12;

	p->_12 = (fx32)(((fx64)x * xx + (fx64)y * yy + (fx64)z * zz) >> FX32_SHIFT);
	p->_11 = (fx32)(((fx64)x * b->_01 + (fx64)y * b->_11 + (fx64)z * b->_21) >> FX32_SHIFT);

	xx = b->_00;
	yy = b->_10;
	zz = b->_20;

	p->_10 = (fx32)(((fx64)x * xx + (fx64)y * yy + (fx64)z * zz) >> FX32_SHIFT);

	// row 2
	x = a->_20;
	y = a->_21;
	z = a->_22;

	p->_20 = (fx32)(((fx64)x * xx + (fx64)y * yy + (fx64)z * zz) >> FX32_SHIFT);
	p->_21 = (fx32)(((fx64)x * b->_01 + (fx64)y * b->_11 + (fx64)z * b->_21) >> FX32_SHIFT);
	xx = b->_02;
	yy = b->_12;
	zz = b->_22;

	p->_22 = (fx32)(((fx64)x * xx + (fx64)y * yy + (fx64)z * zz) >> FX32_SHIFT);

	// row 3
	x = a->_30;
	y = a->_31;
	z = a->_32;

	p->_32 = (fx32)((((fx64)x * xx + (fx64)y * yy + (fx64)z * zz) >> FX32_SHIFT) + b->_32);
	p->_31 = (fx32)((((fx64)x * b->_01 + (fx64)y * b->_11 + (fx64)z * b->_21) >> FX32_SHIFT) + b->_31);
	p->_30 = (fx32)((((fx64)x * b->_00 + (fx64)y * b->_10 + (fx64)z * b->_20) >> FX32_SHIFT) + b->_30);

	if(p == &tmp)
		*ab = tmp;
}

void MTX_RotZ44(MtxFx44 *pDst, fx32 sinVal, fx32 cosVal)
{
	pDst->_00 = cosVal;
	pDst->_01 = sinVal;
	pDst->_10 = -sinVal;
	pDst->_11 = cosVal;

	pDst->_02 = 0;
	pDst->_03 = 0;
	pDst->_12 = 0;
	pDst->_13 = 0;
	pDst->_20 = 0;
	pDst->_21 = 0;
	pDst->_22 = FX32_CONST(1.0);
	pDst->_23 = 0;
	pDst->_30 = 0;
	pDst->_31 = 0;
	pDst->_32 = 0;
	pDst->_33 = FX32_CONST(1.0);
}
