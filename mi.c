#include <string.h>

#include "mi.h"

void MI_Copy48B(const void* pSrc, void* pDest)
{
	memcpy(pDest, pSrc, 48);
}
