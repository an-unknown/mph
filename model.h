#ifndef __MODEL_H__
#define __MODEL_H__

enum CULL_MODE {
	DOUBLE_SIDED = 0x0,
	BACK_SIDE = 0x1,
	FRONT_SIDE = 0x2
};

enum POLYGON_MODE {
	MODULATION = 0x0,
	DECAL = 0x1,
	HIGHLIGHT = 0x2,
	SHADOW = 0x3
};

enum REPEAT_MODE {
	CLAMP = 0x0,
	REPEAT = 0x1,
	MIRROR = 0x2
};

#define RELOAD_MODEL		0x02
#define USE_COMPRESSION		0x04
#define USE_ARCHIVE		0x08
#define USE_EXTERNAL_TXTR	0x40

#define	USE_LIGHT		0x1

typedef struct {
	u8	format;
	u8	field_1;
	u16	width;
	u16	height;
	s16	field_6;
	u8*	image;
	s32	image_size;
	void*	field_10;
	s32	field_14;
	u32	vram_offset;
	s32	opaque;
	s32	some_value;
	u8	packed_size;
	u8	native_texture_format;
	s16	texture_obj_ref;
} Texture;

typedef struct {
	u32*	entries;
	u32	count;
	u32	field_8;
	u32	some_reference;
} Palette;

typedef struct {
	u16	material_id;
	u16	dlist_id;
} Mesh;

typedef struct {
	char	name[64];
	u8	light;
	u8	culling;
	u8	alpha;
	u8	wireframe;
	s16	palid;
	s16	texid;
	u8	x_repeat;
	u8	y_repeat;
	Color3	diffuse;
	Color3	ambient;
	Color3	specular;
	u8	field_53;
	s32	polygon_mode;
	u8	polygon_id;
	u8	anim_flags;
	s16	field_5A;
	s32	texcoord_transform_mode;
	s16	texcoord_animation_id;
	s16	field_62;
	s32	matrix_id;
	s32	scale_s;
	s32	scale_t;
	u16	rot_z;
	s16	field_72;
	s32	scale_width;
	s32	scale_height;
	s16	material_animation_id;
	s16	field_7E;
	u8	packed_repeat_mode;
	u8	field_81;
	s16	field_82;
} Material;

typedef struct {
	u32*	data;
	u32	size;
	fx32	bounds[3][2];
} DisplayList;

typedef struct
{
	char	name[64];
	s16	parent;
	s16	child;
	s16	next;
	s16	field_46;
	s32	enabled;
	s16	mesh_count;
	s16	mesh_id;
	s32	field_50;
	s32	field_54;
	s32	field_58;
	u16	field_5C;
	u16	field_5E;
	u16	field_60;
	s16	field_62;
	s32	field_64;
	s32	field_68;
	s32	field_6C;
	s32	field_70;
	VecFx32	vec1;
	VecFx32	vec2;
	u8	type;
	s8	field_8D;
	s16	field_8E;
	MtxFx43	node_transform;
	s32	field_C0;
	s32	field_C4;
	s32	field_C8;
	s32	field_CC;
	s32	field_D0;
	s32	field_D4;
	s32	field_D8;
	s32	field_DC;
	s32	field_E0;
	s32	field_E4;
	s32	field_E8;
	s32	field_EC;
} Node;

typedef struct {
	char	material_name[32];
	u8	scale_s_blend_factor;
	u8	scale_t_blend_factor;
	u16	scale_s_lut_length;
	u16	scale_t_lut_length;
	u16	scale_s_lut_idx;
	u16	scale_t_lut_idx;
	u8	rot_z_blend_factor;
	u8	field_2B;
	u16	rot_z_lut_length;
	u16	rot_z_lut_idx;
	u8	scale_width_blend_factor;
	u8	scale_height_blend_factor;
	u16	scale_width_lut_length;
	u16	scale_height_lut_length;
	u16	scale_width_lut_idx;
	u16	scale_height_lut_idx;
	s16	field_3A;
} TexcoordAnimation;

typedef struct {
	char	material_name[64];
	s32	field_40;
	u8	diffuse_r_blend_factor;
	u8	diffuse_g_blend_factor;
	u8	diffuse_b_blend_factor;
	u8	field_47;
	u16	diffuse_r_lut_length;
	u16	diffuse_g_lut_length;
	u16	diffuse_b_lut_length;
	u16	diffuse_r_lut_start_idx;
	u16	diffuse_g_lut_start_idx;
	u16	diffuse_b_lut_start_idx;
	u8	ambient_r_blend_factor;
	u8	ambient_g_blend_factor;
	u8	ambient_b_blend_factor;
	u8	field_57;
	u16	ambient_r_lut_length;
	u16	ambient_g_lut_length;
	u16	ambient_b_lut_length;
	u16	ambient_r_lut_start_idx;
	u16	ambient_g_lut_start_idx;
	u16	ambient_b_lut_start_idx;
	u8	specular_r_blend_factor;
	u8	specular_g_blend_factor;
	u8	specular_b_blend_factor;
	u8	field_67;
	u16	specular_r_lut_length;
	u16	specular_g_lut_length;
	u16	specular_b_lut_length;
	u16	specular_r_lut_start_idx;
	u16	specular_g_lut_start_idx;
	u16	specular_b_lut_start_idx;
	s32	field_74;
	s32	field_78;
	s32	field_7C;
	s32	field_80;
	u8	alpha_blend_factor;
	u8	field_85;
	u16	alpha_lut_length;
	u16	alpha_lut_start_idx;
	u16	material_id;
} MaterialAnimation;

typedef struct {
	char	material_name[32];
	u16	count;
	u16	start_idx;
	s16	min_palid;
	u16	material_id;
	s16	min_texid;
	s16	field_2A;
} TextureAnimation;

typedef struct {
	u32	initial_frame;
	fx32*	scale_texcoord_lut;
	u16*	rot_lut;
	fx32*	scale_size_lut;
	u32	material_animation_count;
	TexcoordAnimation* texcoord_animations;
	u16	anim_frame;
	s16	field_1A;
	u32	field_1C;
} TexcoordAnimationData;

typedef struct {
	u32	initial_frame;
	u8*	color_lut;
	u32	material_animation_count;
	MaterialAnimation* material_animation;
	u16	anim_frame;
	s16	field_12;
	u32	field_14;
	u32	field_18;
	u32	field_1C;
	u32	field_20;
	u32	field_24;
	u32	field_28;
	u32	field_2C;
	u32	field_30;
	u32	field_34;
	u32	field_38;
	u32	field_3C;
	u32	field_40;
	u32	field_44;
	u32	field_48;
	u32	field_4C;
	u32	field_50;
	u32	field_54;
	u32	field_58;
	u32	field_5C;
	u32	field_60;
	u32	field_64;
	u32	field_68;
	u32	field_6C;
	u32	field_70;
	u32	field_74;
	u32	field_78;
	u32	field_7C;
	u32	field_80;
	u32	field_84;
	u32	field_88;
} MaterialAnimationData;

typedef struct {
	u32	initial_frame;
	u32	field_4;
	u16	texture_animation_count;
	s16	field_A;
	u16*	frames;
	u16*	texids;
	s16*	pals;
	TextureAnimation* texture_animations;
	u16	anim_frame;
	s16	field_1E;
	u32	field_20;
	u32	field_24;
	u32	field_28;
} TextureAnimationData;

typedef struct {
	s32		modelview_mtx_shamt;
	fx32		scale;
	s32		unk3;
	s32		unk4;
	Material*	materials;
	DisplayList*	dlists;
	Node*		nodes;
	u16		unk_anim_count;
	u8		flags;
	u8		field_1F;
	s32*		some_node_id;
	Mesh*		meshes;
	u16		texture_count;
	s16		field_2A;
	Texture*	textures;
	u16		palette_count;
	s16		field_32;
	Palette*	palettes;
	s32*		some_anim_counts;
	s32*		unk8;
	VecFx32*	node_initial_pos;
	VecFx32*	node_pos;
	u16		num_materials;
	u16		num_nodes;
	MtxFx44*	texture_matrices;
	void*		node_animation;
	TexcoordAnimationData* texcoord_animations;
	MaterialAnimationData* material_animations;
	TextureAnimationData* texture_animations;
	u16		num_meshes;
	u16		num_texture_matrices;
} Model;

/* functions */
void load_model(Model** modelptr, const char* filename, int flags);
void setup_textures_and_palettes(Model* model, const void* texture_base, const char* filename, int flags);
int get_total_texture_size(Model* model);

char* get_room_node_name(Model* model);
int get_node_child(const char* name, Model* model);

void draw_single_node(Model* model, Node* node, int alpha_scale);
void draw_node_tree_non_T(Model* model, int node_idx);

extern int (*dynamic_setup_material)(Model* model, int material_id, int alpha_scale);

#endif
