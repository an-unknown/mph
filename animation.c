#include <stdint.h>
#include <string.h>

#include "types.h"
#include "io.h"
#include "heap.h"
#include "model.h"
#include "animation.h"

#define MAKEPTR(x, base) { \
	if(x) { \
		(x) = (void*) ((uintptr_t) (x) + (uintptr_t) (base)); \
	} \
}

void parse_node_animation(Node* nodes, int node_cnt, NodeAnimationData* node_anims)
{
	int i;
	for(i = 0; i < node_cnt; i++) {
		NodeAnimation* anim = &node_anims->node_animations[i];
		anim->flags = 0;

		if(anim->field_4 == 1 && anim->field_6 == 1 && anim->field_8 == 1) {
			fx32* ptr = node_anims->fx32_ptr;
			if(ptr[anim->field_A] == FX32_CONST(1.0) && ptr[anim->field_C] == FX32_CONST(1.0) && ptr[anim->field_E] == FX32_CONST(1.0))
				anim->flags |= 2;
		}
		if(anim->field_24 == 1 && anim->field_26 == 1 && anim->field_28 == 1) {
			int* ptr = node_anims->int_ptr;
			if(!ptr[anim->field_2A] && !ptr[anim->field_2C] && !ptr[anim->field_2E])
				anim->flags |= 8;
		}
		if(anim->field_14 == 1 && anim->field_16 == 1 && anim->field_18 == 1) {
			u16* ptr = node_anims->short_ptr;
			if(!ptr[anim->field_1A] && !ptr[anim->field_1C] && !ptr[anim->field_1E])
				anim->flags |= 4;
		}
		if(anim->flags & 2 && anim->flags & 4 && anim->flags & 8)
			anim->flags = anim->flags & 0xFE | 1;
		if(anim->field_0 == 1 && anim->field_1 == 1 && anim->field_2 == 1 &&
				anim->field_10 == 1 && anim->field_11 == 1 && anim->field_12 == 1 &&
				anim->field_20 == 1 && anim->field_21 == 1 && anim->field_22 == 1)
			anim->flags |= 0x10;
	}
}

static void parse_material_animation(Model* model, MaterialAnimationData* animation)
{
	int i;
	int j;

	animation->anim_frame = 0;
	model->material_animations = animation;

	for(i = 0; i < model->num_materials; i++) {
		Material* material = &model->materials[i];
		material->material_animation_id = -1;
		for(j = 0; j < animation->material_animation_count; j++) {
			MaterialAnimation* anim = &animation->material_animation[i];
			if(!strcmp(material->name, anim->material_name)) {
				material->material_animation_id = j;
				animation->material_animation[j].material_id = i;
				break;
			}
		}
	}
}

static void parse_texcoord_animation(Model* model, TexcoordAnimationData* animation)
{
	int i;
	int j;

	animation->anim_frame = 0;
	model->texcoord_animations = animation;
	TexcoordAnimation* animations = animation->texcoord_animations;

	for(i = 0; i < model->num_materials; i++) {
		model->materials[i].texcoord_animation_id = -1;
		Material* material = &model->materials[i];
		for(j = 0; j < animation->material_animation_count; j++) {
			if(!strcmp(material->name, animations[j].material_name)) {
				model->materials[i].texcoord_animation_id = j;
				if(material->texcoord_transform_mode == GX_TEXGEN_NONE)
					material->texcoord_transform_mode = GX_TEXGEN_TEXCOORD;
				break;
			}
		}
	}
}

static void parse_texture_animation(Model* model, TextureAnimationData* animation)
{
	int i;
	int j;

	animation->anim_frame = 0;
	model->texture_animations = animation;
	for(i = 0; i < model->num_materials; i++) {
		Material* mtl = &model->materials[i];
		mtl->field_7E = -1;
		for(j = 0; j < animation->texture_animation_count; j++) {
			TextureAnimation* anim = &animation->texture_animations[j];
			if(!strcmp(mtl->name, anim->material_name)) {
				int k;
				int start_idx = anim->start_idx;
				int first_id = animation->texids[start_idx];
				for(k = 1; k < anim->count; k++) {
					int id = animation->texids[start_idx + k];
					if(id < first_id)
						first_id = id;
				}
				anim->min_palid = mtl->palid - first_id;
				anim->min_texid = mtl->texid - first_id;
				anim->material_id = i;
				break;
			}
		}
	}
}

void parse_animation(Animation* animation, Model* model)
{
	unsigned int i;

	MAKEPTR(animation->node_animations, animation);
	MAKEPTR(animation->field_4, animation);
	MAKEPTR(animation->material_animations, animation);
	MAKEPTR(animation->texcoord_animations, animation);
	MAKEPTR(animation->texture_animations, animation);

	for(i = 0; i < animation->count; i++) {
		MAKEPTR(animation->node_animations[i], animation);
		MAKEPTR(animation->field_4[i], animation);
		MAKEPTR(animation->material_animations[i], animation);
		MAKEPTR(animation->texcoord_animations[i], animation);
		MAKEPTR(animation->texture_animations[i], animation);

		if(animation->node_animations[i]) {
			NodeAnimationData* anim = animation->node_animations[i];
			MAKEPTR(anim->fx32_ptr, animation);
			MAKEPTR(anim->short_ptr, animation);
			MAKEPTR(anim->int_ptr, animation);
			MAKEPTR(anim->node_animations, animation);
			parse_node_animation(model->nodes, model->num_nodes, anim);
		}

		if(animation->material_animations[i]) {
			MaterialAnimationData* anim = animation->material_animations[i];
			MAKEPTR(anim->color_lut, animation);
			MAKEPTR(anim->material_animation, animation);
			parse_material_animation(model, anim);
		}

		if(animation->texcoord_animations[i]) {
			TexcoordAnimationData* anim = animation->texcoord_animations[i];
			MAKEPTR(anim->scale_texcoord_lut, animation);
			MAKEPTR(anim->rot_lut, animation);
			MAKEPTR(anim->scale_size_lut, animation);
			MAKEPTR(anim->texcoord_animations, animation);
			parse_texcoord_animation(model, anim);
		}

		if(animation->texture_animations[i]) {
			TextureAnimationData* anim = animation->texture_animations[i];
			MAKEPTR(anim->frames, animation);
			MAKEPTR(anim->texids, animation);
			MAKEPTR(anim->pals, animation);
			MAKEPTR(anim->texture_animations, animation);
			parse_texture_animation(model, anim);
		}
	}
}

void load_animation(Animation** animation, const char* filename, Model* model, char flags)
{
	check_heap();
	if(flags & USE_COMPRESSION) {
		/* LoadFileCmpr((void**) animation, filename); */
	} else if(flags & USE_ARCHIVE) {
		LoadFileFromArchive((void**) animation, filename);
	} else {
		LoadFile((void**) animation, filename);
	}
	parse_animation(*animation, model);
}
