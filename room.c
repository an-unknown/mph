#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "types.h"
#include "heap.h"
#include "error.h"
#include "io.h"
#include "model.h"
#include "animation.h"
#include "collision.h"
#include "entity.h"
#include "rooms.h"
#include "room.h"

#include "state.h"

u16 room_layer_mask;
u8* current_texture_image;
u32 current_texture_size;
int modelview_mtx_shamt;

NodeRef* node_refs;
NodeRef nodes_list;
NodeRef room_node_refs;

int nodes_count;

void unlink_node_ref(NodeRef* node_ref)
{
	node_ref->next->prev = node_ref->prev;
	node_ref->prev->next = node_ref->next;
}

void insert_node(NodeRef* prev, NodeRef* node_ref)
{
	node_ref->next = prev->next;
	node_ref->prev = prev;
	prev->next->prev = node_ref;
	prev->next = node_ref;
}

NodeRef* remove_first_node()
{
	NodeRef* old_head = NULL;
	if(nodes_list.next != &nodes_list) {
		old_head = nodes_list.next;
		unlink_node_ref(nodes_list.next);
	}
	return old_head;
}

static void filter_nodes(Model* model)
{
	unsigned int i;
	for(i = 0; i < model->num_nodes; i++) {
		Node* node = &model->nodes[i];
		int flags = 0;
		if(strlen(node->name)) {
			unsigned int p;
			int keep = 0;
			for(p = 0; p < strlen(node->name); p += 4) {
				char* ch1 = &node->name[p];
				if(*ch1 != '_')
					break;
				if(*(u16*)ch1 == *(u16*)"_s") {
					int nr = node->name[p + 3] - '0' + 10 * (node->name[p + 2] - '0');
					if(nr)
						flags = flags & 0xC03F | ((((u32)flags << 18 >> 24) | (1 << nr)) << 6);
				}
				u32 val32 = *(u32*) ch1;
				if(val32 == *(u32*)"_ml0")
					flags |= LAYER_ML0;
				else if(val32 == *(u32*)"_ml1")
					flags |= LAYER_ML1;
				else if(val32 == *(u32*)"_mpu")
					flags |= LAYER_MPU;
				else if(val32 == *(u32*)"_ctf")
					flags |= LAYER_CTF;
			}

			if(!p || flags & room_layer_mask)
				keep = 1;
			if(!keep)
				node->enabled = 0;
		}
	}
}

static void translate_nodes(Model* model, VecFx32 offset)
{
}

void get_room_node(NodeRef* node_ref, Room* room, char* room_node_name)
{
	char buf[0x100];

	node_ref->ports = NULL;
	node_ref->room_node_name = room_node_name;
	node_ref->room = room;
	node_ref->room_node_id = get_node_child(node_ref->room_node_name, room->model);
	if(node_ref->room_node_id < 0) {
		sprintf(buf, "could not find node to draw of room : %s!!\n", node_ref->room_node_name);
		fatal_error(buf);
	}
	insert_node(room_node_refs.prev, node_ref);
	nodes_count++;
}

Room* load_room(const RoomDescription* room_descr, int which_list, int some_value, fx32 x, fx32 y, fx32 z, char* room_name)
{
	unsigned int i;
	char filename[256];

	if(!some_value) {
		room_layer_mask = 0;
		if(room_descr->layer_id) {
			room_layer_mask = room_layer_mask & 0xC03F | (((1 << room_descr->layer_id) & 0xFF) << 6);
		}
	}

	Room* room = alloc_from_heap(sizeof(Room));
	room->some_value = some_value;
	room->collision = NULL;
	room->room_descr = room_descr;

	if(some_value) {
		char* name = room_name;
		if(!room_name)
			room_name = "Con01";
		sprintf(room->name, name);
	} else {
		sprintf(room->name, "%s", room_descr->name);
	}

	room->pos.x = x;
	room->pos.y = y;
	room->pos.z = z;

	int flags = USE_ARCHIVE;
	sprintf(filename, "%s/%s", room_descr->internal_name, room_descr->model);
	if(room_descr->tex)
		flags = 0x20 | USE_EXTERNAL_TXTR | USE_ARCHIVE;
	load_model(&room->model, filename, flags);
	filter_nodes(room->model);

	if(room_descr->tex) {
		void* txtr_file = 0;
		check_heap();
		sprintf(filename, "levels/textures/%s", room_descr->tex);
		LoadFile(&txtr_file, filename);
		setup_textures_and_palettes(room->model, txtr_file, filename, flags);
		free_to_heap(txtr_file);
	}

	if(!some_value) {
		current_texture_image = room->model->textures->image;
		current_texture_size = get_total_texture_size(room->model);
	}

	VecFx32 tmpvec;
	tmpvec.x = x;
	tmpvec.y = y;
	tmpvec.z = z;
	translate_nodes(room->model, tmpvec);

	if(!some_value)
		modelview_mtx_shamt = room->model->modelview_mtx_shamt;

	room->cmaterials = alloc_from_heap(room->model->num_materials * sizeof(CMaterial));

	if(room_descr->anim) {
		sprintf(filename, "%s/%s", room_descr->internal_name, room_descr->anim);
		load_animation(&room->animation, filename, room->model, USE_ARCHIVE);
	}
	/* CModel_setup_animation(&room->cmodel, room->model, room->animation); */

	if(room_descr->collision) {
		sprintf(filename, "%s/%s", room_descr->internal_name, room_descr->collision);
		load_collision(&room->collision, filename, USE_ARCHIVE);
		translate_collision_data(room->collision, x, y, z);
	}

	if(room_descr->ent) {
		int layer_id = 0;
		/* magic */
		/* layer_id = game_state.something_with_layer_id; */
		sprintf(filename, "levels/entities/%s", room_descr->ent);
		EntLoad(&entities, filename, layer_id);
	}

	/* ... */
	int active_port_count = 0;
	if(room->collision) {
		Collision* collision = room->collision;
		for(i = 0; i < collision->port_count; i++) {
			CollisionPort* port = &collision->ports[i];
			if(port->layer_mask & 4 || port->layer_mask & room_layer_mask) {
				setup_port_nodes(port, room);
				active_port_count++;
			}
		}
	}

	if(!active_port_count) {
		char* room_node_name = NULL;
		NodeRef* node_ref = remove_first_node();
		if(room_descr->room_node_name && get_node_child(room_descr->room_node_name, room->model) != -1)
			room_node_name = room_descr->room_node_name;
		if(!room_node_name)
			room_node_name = get_room_node_name(room->model);
		get_room_node(node_ref, room, room_node_name);
	}
}

Room* load_current_room()
{
	/* ... */
	return load_room(&rooms[game_state.room_id], 0, 0, 0, 0, 0, 0);
}

void initialize_current_room()
{
	unsigned int i;

	/* ... */
	node_refs = (NodeRef*) alloc_from_heap(NUM_NODE_REFS * sizeof(NodeRef));

	/* ... */
	nodes_count = 0;
	room_node_refs.next = &room_node_refs;
	room_node_refs.prev = &room_node_refs;
	nodes_list.next = &nodes_list;
	nodes_list.prev = &nodes_list;

	for(i = 0; i < NUM_NODE_REFS; i++) {
		node_refs[i].room_node_id_maybe = i;
		insert_node(nodes_list.prev, &node_refs[i]);
	}

	load_current_room();
}
