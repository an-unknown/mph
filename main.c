#include <stdio.h>
#include <stddef.h>

#include "types.h"
#include "rooms.h"
#include "room.h"
#include "model.h"
#include "texture_containers.h"
#include "pickup_models.h"
#include "state.h"

int main(void)
{
	game_state.room_id = 33; /* single player version of "Combat Hall" */
	initialize_current_room();

	NodeRef* room_node_ref = room_node_refs.next;
	Room* room = room_node_ref->room;
	printf("root node: '%s'\n", room_node_ref->room_node_name);
	printf("animations: %d\n", room->animation->count);
	printf("materials: %d\n", room->model->num_materials);
}
