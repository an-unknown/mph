#include <stdint.h>
#include <string.h>

#include "error.h"
#include "io.h"
#include "heap.h"
#include "os.h"
#include "fxmath.h"
#include "mi.h"
#include "model.h"
#include "dlist.h"

#define MAKEPTR(x, base) { \
	if(x) { \
		(x) = (void*) ((uintptr_t) (x) + (uintptr_t) (base)); \
	} \
}

int total_dlist_processed_data;
int (*dynamic_setup_material)(Model* model, int material_id, int alpha_scale);

static int get_txtr_repeat_mode(int x_repeat, int y_repeat)
{
	int result = 0;
	if(x_repeat)
		result |= 1;
	if(y_repeat)
		result |= 2;
	return result;

}

static int get_txtr_flip_mode(int x_repeat, int y_repeat)
{
	int result = 0;
	if(x_repeat == MIRROR)
		result |= 1;
	if(y_repeat == MIRROR)
		result |= 2;
	return result;
}

static int get_txtr_format(int format)
{
	switch(format) {
		case 0:
			return GX_TEXFMT_PLTT4;
		case 1:
			return GX_TEXFMT_PLTT16;
		case 2:
			return GX_TEXFMT_PLTT256;
		case 3:
			return GX_TEXFMT_COMP4x4;
		case 4:
			return GX_TEXFMT_A5I3;
		case 5:
			return GX_TEXFMT_DIRECT;
		case 6:
			return GX_TEXFMT_A3I5;
		default:
			return GX_TEXFMT_NONE;
	}
}

static int get_txtr_width(int width)
{
	switch(width) {
		case 8:
			return GX_TEXSIZE_S8;
		case 16:
			return GX_TEXSIZE_S16;
		case 32:
			return GX_TEXSIZE_S32;
		case 64:
			return GX_TEXSIZE_S64;
		case 128:
			return GX_TEXSIZE_S128;
		case 256:
			return GX_TEXSIZE_S256;
		case 512:
			return GX_TEXSIZE_S512;
		default:
			return GX_TEXSIZE_S1024;
	}
}

static int get_txtr_height(int height)
{
	switch(height) {
		case 8:
			return GX_TEXSIZE_T8;
		case 16:
			return GX_TEXSIZE_T16;
		case 32:
			return GX_TEXSIZE_T32;
		case 64:
			return GX_TEXSIZE_T64;
		case 128:
			return GX_TEXSIZE_T128;
		case 256:
			return GX_TEXSIZE_T256;
		case 512:
			return GX_TEXSIZE_T512;
		default:
			return GX_TEXSIZE_T1024;
	}
}

static void parse_nodes(Model* model, int start_idx)
{
	MtxFx43 mtx1;
	int idx = start_idx;
	while(idx != -1) {
		Node* node = &model->nodes[idx];
		int parent_id = node->parent;
		int child_id = node->child;

		if(parent_id == -1)
			MI_Copy48B(&mtx1, &node->node_transform);
		else
			MTX_Concat43(&mtx1, &model->nodes[parent_id].node_transform, &node->node_transform);

		if(child_id != -1)
			parse_nodes(model, child_id);

		if(model->node_pos) {
			MTX_Identity43(&mtx1);
			mtx1.m[3][0] = -model->node_initial_pos[idx].x;
			mtx1.m[3][1] = -model->node_initial_pos[idx].y;
			mtx1.m[3][2] = -model->node_initial_pos[idx].z;
		}
		idx = node->next;
	}
}

static void setup_texcoord_matrix(MtxFx44 *texcoord_mtx, fx32 scale_s, fx32 scale_t, int rot_z, int scale_width, int scale_height, u16 width, u16 height)
{
	int scaled_width = width * scale_width;
	int scaled_height = height * scale_height;

	texcoord_mtx->_20 = 0;
	texcoord_mtx->_21 = 0;

	if(rot_z) {
		fx16 sinVal = FX_SinIdx(rot_z);
		fx16 cosVal = FX_CosIdx(rot_z);

		fx32 width_half = FX32_CONST(width / 2);
		fx32 height_half = FX32_CONST(height / 2);

		MTX_RotZ44(texcoord_mtx, sinVal, cosVal);

		texcoord_mtx->_30 = FX_MUL(sinVal, width_half) - FX_MUL(cosVal, width_half) + width_half;
		texcoord_mtx->_31 = -FX_MUL(sinVal, height_half) + FX_MUL(cosVal, height_half) + height_half;

		if(scale_s == FX32_CONST(1)) {
			texcoord_mtx->_30 += scaled_width;
		} else {
			texcoord_mtx->_00 = FX_MUL(texcoord_mtx->_00, scale_s);
			texcoord_mtx->_10 = FX_MUL(texcoord_mtx->_10, scale_s);
			texcoord_mtx->_30 = FX_MUL(texcoord_mtx->_30, scale_s);
			texcoord_mtx->_30 += FX_MUL(scaled_width, scale_s);
		}

		if(scale_t == FX32_CONST(1)) {
			texcoord_mtx->_31 += scaled_height;
		} else {
			texcoord_mtx->_01 = FX_MUL(texcoord_mtx->_01, scale_t);
			texcoord_mtx->_11 = FX_MUL(texcoord_mtx->_11, scale_t);
			texcoord_mtx->_31 = FX_MUL(texcoord_mtx->_31, scale_t);
			texcoord_mtx->_31 += FX_MUL(scaled_height, scale_t);
		}
	} else {
		texcoord_mtx->_00 = scale_s;
		texcoord_mtx->_01 = 0;
		texcoord_mtx->_10 = 0;
		texcoord_mtx->_11 = scale_t;

		if(scale_s == FX32_CONST(1)) {
			texcoord_mtx->_30 = scaled_width;
		} else {
			texcoord_mtx->_30 = FX_MUL(scaled_width, scale_s);
		}

		if(scale_t == FX32_CONST(1)) {
			texcoord_mtx->_31 = scaled_height;
		} else {
			texcoord_mtx->_31 = FX_MUL(scaled_height, scale_t);
		}
	}
}

static void parse_materials(Model* model)
{
	int i;
	if(model->texture_matrices) {
		for(i = 0; i < model->num_materials; i++) {
			Material* material = &model->materials[i];
			if(material->texcoord_transform_mode) {
				int width;
				int height;
				if(model->textures) {
					Texture* tex = &model->textures[material->texid];
					width = tex->width;
					height = tex->height;
				} else {
					width = 1;
					height = 1;
				}

				setup_texcoord_matrix(&model->texture_matrices[material->matrix_id],
						material->scale_s,
						material->scale_t,
						material->rot_z,
						material->scale_width,
						material->scale_height,
						width,
						height);
			}
		}
	}
}

static void parse_textures(Texture* textures, unsigned int count, const void* texture_base)
{
	unsigned int i;
	for(i = 0; i < count; i++) {
		Texture* tex = &textures[i];
		MAKEPTR(tex->image, texture_base);
		if(tex->field_10)
			MAKEPTR(tex->field_10, texture_base);
		tex->native_texture_format = get_txtr_format(tex->format);
		tex->packed_size = get_txtr_width(tex->width) << 4;
		tex->packed_size |= get_txtr_height(tex->height);
	}
}

static void parse_palettes(Palette* palettes, unsigned int count, const void* texture_base)
{
	unsigned int i;
	for(i = 0; i < count; i++) {
		Palette* pal = &palettes[i];
		MAKEPTR(pal->entries, texture_base);
	}
}

static void transfer_textures_to_vram(Texture* textures, int count, int flags)
{
	unsigned int i;
	for(i = 0; i < count; i++) {
		Texture* tex = &textures[i];
		if(!tex->some_value) {
			/* upload */
			/* if flags & 0x20: store obj ref somewhere */
		}
	}
}

static void transfer_palettes_to_vram(Palette* palettes, int count, int flags)
{
}

static void transfer_to_vram(const char* filename, Model* model, int flags)
{
	transfer_textures_to_vram(model->textures, model->texture_count, flags);
	transfer_palettes_to_vram(model->palettes, model->palette_count, flags);
}

void setup_textures_and_palettes(Model* model, const void* texture_base, const char* filename, int flags)
{
	parse_textures(model->textures, model->texture_count, texture_base);
	parse_palettes(model->palettes, model->palette_count, texture_base);
	if(!(flags & RELOAD_MODEL)) {
		transfer_to_vram(filename, model, flags);
	}
}

static void parse_model(const char* name, Model* model, char flags, void* texture_base)
{
	int i;

	MAKEPTR(model->materials, texture_base);
	MAKEPTR(model->textures, texture_base);
	MAKEPTR(model->palettes, texture_base);
	MAKEPTR(model->dlists, model);
	MAKEPTR(model->nodes, model);
	MAKEPTR(model->some_node_id, model);
	MAKEPTR(model->meshes, model);
	MAKEPTR(model->some_anim_counts, model);
	MAKEPTR(model->unk8, model);
	MAKEPTR(model->node_pos, model);
	MAKEPTR(model->node_initial_pos, model);
	MAKEPTR(model->node_animation, model);
	MAKEPTR(model->texcoord_animations, model);
	MAKEPTR(model->material_animations, model);
	MAKEPTR(model->texture_animations, model);

	/* fixup display lists */
	for(i = 0; i < model->num_meshes; i++) {
		DisplayList* dlist = &model->dlists[i];
		MAKEPTR(dlist->data, model);
	}

	if(model->num_texture_matrices) {
		if(model->texture_matrices && (flags & RELOAD_MODEL)) {
			free_to_heap(model->texture_matrices);
		}
		model->texture_matrices = (MtxFx44*) alloc_from_heap(model->num_texture_matrices * sizeof(MtxFx44));
		if(!model->texture_matrices) {
			OS_Terminate();
		}
	}

	for(i = 0; i < model->num_materials; i++) {
		Material* material = &model->materials[i];
		material->anim_flags = 0;
		material->packed_repeat_mode = get_txtr_repeat_mode(material->x_repeat, material->y_repeat) << 4;
		material->packed_repeat_mode |= get_txtr_flip_mode(material->x_repeat, material->y_repeat);
		if(material->light)
			model->flags |= USE_LIGHT;
	}

	parse_nodes(model, 0);

	parse_materials(model);

	for(i = 0; i < model->num_nodes; i++) {
		model->nodes[i].field_C0 = 0;
		model->nodes[i].field_C4 = 0;
	}
}

void load_model(Model** modelptr, const char* filename, int flags)
{
	check_heap();
	if(flags & RELOAD_MODEL) {
		/* LoadFile0(*modelptr, filename); */
		LoadFile((void**) modelptr, filename);
	} else if(flags & USE_COMPRESSION) {
		/* LoadFileCmpr((void**) modelptr, filename); */
		LoadFile((void**) modelptr, filename);
	} else if(flags & USE_ARCHIVE) {
		LoadFileFromArchive((void**) modelptr, filename);
	} else {
		LoadFile((void**) modelptr, filename);
	}
	parse_model(filename, *modelptr, flags, *modelptr);

	if(!(flags & USE_EXTERNAL_TXTR))
		setup_textures_and_palettes(*modelptr, *modelptr, filename, flags);
}

int get_total_texture_size(Model* model)
{
	int i;
	int size = 0;
	for(i = 0; i < model->texture_count; i++)
		size += model->textures[i].image_size;
	return size;
}

char* get_room_node_name(Model* model)
{
	unsigned int i;
	for(i = 0; i < model->num_nodes; i++) {
		Node* node = &model->nodes[i];
		if(node->name[0] == 'r' && node->name[1] == 'm')
			return node->name;
	}
	return "rmMain";
}

int get_node_child(const char* name, Model* model)
{
	unsigned int i;
	if(model->num_nodes <= 0)
		return -1;
	for(i = 0; i < model->num_nodes; i++) {
		Node* node = &model->nodes[i];
		if(!strcmp(node->name, name))
			return node->child;
	}
	return -1;
}

void draw_single_node(Model* model, Node* node, int alpha_scale)
{
	unsigned int i;
	if(node->mesh_count > 0 && node->enabled == 1) {
		int mesh_id = node->mesh_id;
		if(mesh_id + 1 < 0)
			fatal_error("ERR: disp_idx < 0\n");

		for(i = 0; i < node->mesh_count; i++) {
			Mesh* mesh = &model->meshes[i];
			if(dynamic_setup_material(model, mesh->material_id, alpha_scale) == 1) {
				DisplayList* dlist = &model->dlists[mesh->dlist_id];
				draw_dlist(dlist);
			}
		}
	}
}

void draw_node_tree_non_T(Model* model, int node_idx)
{
	unsigned int i;
	while(node_idx != -1) {
		Node* node = &model->nodes[node_idx];
		if(node->mesh_count > 0 && node->enabled) {
			int mesh_id = node->mesh_id;
			if(mesh_id + 1 < 0)
				fatal_error("ERR: disp_idx < 0\n");
			for(i = 0; i < node->mesh_count; i++) {
				Mesh* mesh = &model->meshes[i];
				int mtl_id = mesh->material_id;
				if(model->materials[mtl_id].name[0] != 'T') {
					DisplayList* dlist = &model->dlists[mesh->dlist_id];
					total_dlist_processed_data += dlist->size;
					draw_dlist(dlist);
				}
			}
		}
		if(node->child != -1)
			draw_node_tree_non_T(model, node->child);
	}
}
